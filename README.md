# Example Docker container for custom FSL tools

This image includes FSL bet and flirt, and all dependencies.

FSL tools can be now [installed separately using Conda](https://open.win.ox.ac.uk/pages/fsl/docs/#/install/conda).

The image is [shrinked using conda-pack](https://pythonspeed.com/articles/conda-docker-image-size/).

It is not easy to put FSL tools in a Docker container, which later can be converted to Apptainer used with Nextflow. The problem is, that FSL requires to source a config file which sets up some environment variables. To make things work, these variables are explicitely set with `ENV` in the Dockerfile.

## Additional resources

- [FSL with conda](https://open.win.ox.ac.uk/pages/fsl/docs/#/install/conda)
- [FSL with Docker](https://open.win.ox.ac.uk/pages/fsl/docs/#/install/container)
- [Conda environments in Docker](https://pythonspeed.com/articles/activate-conda-dockerfile/)
- [Conversion compatibility between Docker and Apptainer](https://hpc-docs.cubi.bihealth.org/how-to/software/apptainer/#conversion-compatibility)
- relevant discussions on FSL mailing list:
   - https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?A2=ind2312&L=FSL&O=D&X=343C8BB7B4038D1F5C&Y=mateusz.pawlik%40plus.ac.at&P=49513
   - https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?A2=ind2312&L=FSL&O=D&X=343C8BB7B4038D1F5C&Y=mateusz.pawlik%40plus.ac.at&P=47689
